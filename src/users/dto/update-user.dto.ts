import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  email: string;
  fullName: string;
  password: string;
  gender: string;
  roles: string;
  image: string;
}
